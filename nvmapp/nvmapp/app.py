import datetime

def candidatesName(first, last):
  return first + last

def simpleMath(x, y):
  return x + y

def year():
  dt = datetime.datetime.today()
  return dt

def letterCheck():  
  this_language = 'python' 
  if 'p' in this_language:
    return True
  else:
    return False

def simpleCount():
  x = 0
  while x < 20:
    x = x + 1
  return x