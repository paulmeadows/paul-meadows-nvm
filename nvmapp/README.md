# NVM CI System Example #

### Getting Started ###

Hello Dave, I understand that you are looking for some guidance and an example for creating a basic CI pipeline. I have put together a very simple Python module to hopefully explain how the steps to create this fit together and to give you something to take away and build upon in a real-world scenario. 

We have a few options for testing and packaging this module. I would say the best way for you to go about this would be to use Docker, however I will explain how you can do this both in Docker and running scripts locally on your machine in case of any limitations. 

### Prerequisites ###

**Installing Docker on Mac or Windows:**

Download Docker Desktop here (choosing your OS):

https://hub.docker.com/?overlay=onboarding

Please note you will need to sign up for a Docker Hub account. This will be used to sign in to Docker Desktop once installation is complete.  

If you are running Windows, there may be limitations depending on your the version of Windows OS you are running. If you are unable to install Docker on Windows due to compatibility issues, the workaround will be to install Docker via Docker Toolbox. Full instructions on installing this can be located here: https://docs.docker.com/v17.09/toolbox/toolbox_install_windows/

If you do not want to install Docker Toolbox, you will still be able to run this CI example locally via the scripts and commands provided.

**Installing docker on Linux:**

CentOS/Red Hat: 

`yum install docker`

Ubuntu: 

`apt-get install docker`

Once you have installed Docker on your machine, launch either terminal or PowerShell and type the following command:

`docker --version`

This should return the current version and build. We are now ready to start using Docker. 

**Installing Python (running locally without Docker):**

**To install Python on Mac or Windows:**

Navigate to https://www.python.org/downloads/release/python-380/ and scroll to the bottom of the page. Locate the correct installer version depending on your OS.

If you are installing on Windows, ensure that you tick "Add Python to PATH" at the beginning of the install. 

Run through the basic install window

**Installing Python on Linux:**

CentOS/Red Hat: 

`yum install python`

Ubuntu: 

`apt-get install python`

Once installed, launch terminal or a PowerShell window and type the following command:

`python --version`

This should return Python and the version number. We are now ready to run this locally.

### Installing ###

Start by cloning the repository to your machine via git or downloading the project manually through the Bitbucket site via the following URL's:

**git clone: git clone https://paulmeadows@bitbucket.org/paulmeadows/paul-meadows-nvm.git**

**Download: https://bitbucket.org/paulmeadows/paul-meadows-nvm/downloads/**

Feel free to clone these files anywhere onto your machine.

### Unit Tests and Package Deployment with Python (running locally without Docker) ###

Firstly, before we run any unit tests or builds, lets understand what we are testing: 

Navigate to nvmapp/nvmapp and open app.py. In here will be several functions that perform basic tasks. 

Navigate to nvmapp/tests and open test_nvmapp. Here you will see we are importing the nvmapp module functions and running several unit tests against these functions. If the result is not correct, ie the candidates name does not match Paul Meadows, the unit test(s) will fail. 

**To perform the unit tests:**

Launch either terminal or PowerShell and navigate to /nvmapp/tests/

Enter the following command: 

`python test_nvmapp.py`

The following output should show 5 successful unit tests.

To build the package:

Launch either terminal or PowerShell and navigate to /nvmapp/

Enter the following command: 

`python setup.py sdist`

The above command with create a folder in the root of the project called dist which will contain the code compressed into a tar.gz format.

### Unit Tests and Package Deployment with Docker ###

Navigate to the root directory of the project folder and open the Dockerfile in a text editor of your choice. 

The Dockerfile will contain all required steps from testing to publishing the package in one execution without the need to install Python on your machine. 

**To complete the unit tests and package build:**

launch terminal or PowerShell and navigate to /nvmapp/

Enter the following command: 

`docker build -t nvmapp .`

You should be able to see each step play out in the terminal or PowerShell window including the unit tests. 

Once the image has finished building, enter the following commands (replacing the local path with one on your machine): 

`docker create -ti --name temp nvmapp bash`

`docker cp temp:/app/dist/NVMApp-0.1.tar.gz /Path/On/Your/Machine/NVMApp-0.1.tar.gz`

The above command should extract the newly build package from the container and place this on your machine.

**To remove the temporary container, run the following command:** 

`docker rm -f temp`

### Authors ###

Paul Meadows

## License ###

Free

