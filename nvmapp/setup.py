from distutils.core import setup

setup(
    name='NVMApp',
    author='Paul Meadows',
    author_email='paulmeadows@outlook.com',
    url='https://bitbucket.org/paulmeadows',  
    version='0.1',
    packages=['nvmapp'],
    license='Free',
    long_description=open('README.md').read()
)