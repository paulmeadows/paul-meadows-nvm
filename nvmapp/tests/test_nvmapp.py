import unittest
import sys
sys.path.append('../')
import nvmapp.app

class unitTestNvmApp(unittest.TestCase):

  def test_candidatesName(self):
    result = nvmapp.app.candidatesName('Paul ', 'Meadows')
    self.assertEqual(result, 'Paul Meadows')

  def test_simpleMath(self):
    result = nvmapp.app.simpleMath(1, 5)
    self.assertEqual(result, 6)

  def test_year(self):
    result = nvmapp.app.year()
    self.assertEqual(result.year, 2019)

  def test_letterCheck(self):
    result = nvmapp.app.letterCheck()
    self.assertEqual(result, True)

  def test_simpleCount(self):
    result = nvmapp.app.simpleCount()
    self.assertEqual(result, 20)

if __name__ == '__main__':
  unittest.main()
